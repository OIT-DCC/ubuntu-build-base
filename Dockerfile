FROM ubuntu:22.04

LABEL maintainer='Mike Newton <jmnewton@duke.edu>'

RUN ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime

RUN apt update && apt install -y build-essential \
    libhttp-parser-dev \
    libyaml-dev \
    libjwt-dev \
    munge \
    libmunge-dev \
    libmariadb-dev \
    mariadb-server-10.6 \
    hwloc \
    libhwloc-dev \
    perl \
    libjson-c-dev \
    libhdf5-dev \
    lua5.4 \
    libnuma-dev \
    numactl \
    libpam0g \
    libpam0g-dev \
    libpmix-dev \
    curl \
    git-all \
    ruby3.0 \
    ruby-rubygems \
    python3 \
    libdbus-1-dev

RUN curl -L https://gitlab.com/gitlab-org/cli/-/releases/v1.39.0/downloads/glab_1.39.0_Linux_x86_64.deb -o glab_1.39.0_Linux_x86_64.deb && dpkg -i ./glab_1.39.0_Linux_x86_64.deb && apt-get install -f

RUN gem install fpm

ENTRYPOINT ["/bin/bash", "-c"]
